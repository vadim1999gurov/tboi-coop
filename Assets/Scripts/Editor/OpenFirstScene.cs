﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEditor;

public class OpenFirstScene : MonoBehaviour
{
    [MenuItem("FirstScene/Open _HOME")]
    private static void OpenScene()
    {
        EditorSceneManager.EnsureUntitledSceneHasBeenSaved("Chto-to");
    }
}
