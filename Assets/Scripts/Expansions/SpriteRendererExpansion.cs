﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Expansions
{

    public static class SpriteRendererExpansion
    {
        public static void SetAlpha(this SpriteRenderer s, float a)
        {
            Color color = new Color();
            color = s.color;
            color.a = a;
            s.color = color;
        }

    }
}
