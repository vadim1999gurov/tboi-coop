﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Expansion
{
    public static class Vector2Expansions
    {
        public static Vector2 Rotate(this Vector2 v, float degrees)
        {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

            float tx = v.x;
            float ty = v.y;
            v.x = (cos * tx) - (sin * ty);
            v.y = (sin * tx) + (cos * ty);
            return v;
        }

        public static Vector2 StepTo(this Vector2 vector, Vector2 direction)
        {
            return new Vector2(vector.x + direction.x, vector.y + direction.y);
        }

    }
}