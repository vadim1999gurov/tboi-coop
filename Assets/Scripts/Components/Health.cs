﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interfaces;
using Delegates;

[DisallowMultipleComponent]
sealed public class Health : MonoBehaviour
{

    #region Events
    public event Call onHealthUpdate;

    #endregion Events

    #region Flags

    // Can pick up heal items?
    [SerializeField]
    private bool isCanHealUp = false;

    #endregion

    #region Parameters

    // Character max health stat.
    [SerializeField]
    [Tooltip("Character max health.")]
    private float maxValue = 10f;
    public float MaxValue
    {
        get => maxValue;
        set => maxValue = value;
    }

    [SerializeField]
    [Tooltip("Character health.")]
    private float value = 0;
    /// <summary>
    /// Character health.
    /// </summary>
    public float Value
    {
        get
        {
            return this.value;
        }
        set
        {
            if (value > 0 && MaxValue >= value)
            {
                this.value = value;
                if (onHealthUpdate != null) onHealthUpdate.Invoke();
            }
            if (MaxValue < value)
            {
                this.value = MaxValue;
                if (onHealthUpdate != null) onHealthUpdate.Invoke();
            }
            else if (value <= 0) gameObject.GetComponent<ILifeObject>()?.Die();
        }
    }
    #endregion Parameters

    #region Unity

    private void OnCollisionEnter2D(Collision2D collider)
    {
        HealUp item;
        if (Value != MaxValue && (item = collider.gameObject.GetComponent<HealUp>()) != null && isCanHealUp) Value += item.GetHealth();
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (onHealthUpdate != null) onHealthUpdate.Invoke();
        if (value <= 0) gameObject.GetComponent<ILifeObject>()?.Die();
    }
#endif

    #endregion Unity
}
