﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class DamageDealer : MonoBehaviour
{
    #region Constants

    // Character min and max damage stat.
    private const float MinValue = 1f;
    private const float MaxValue = 10f;

    #endregion Constants

    #region Parameters

    [SerializeField]
    [Tooltip("Character damage.")]
    private float value;
    /// <summary>
    /// Character damage.
    /// </summary>
    public float Value
    {
        get
        {
            return this.value;
        }
        set
        {
            if (MinValue <= value && value >= MaxValue) this.value = value;
        }
    }

    [SerializeField]
    [Tooltip("Character damage.")]
    private float knockBackValue = 1;
    /// <summary>
    /// Character knock-back.
    /// </summary>
    public float KnockBackValue
    {
        get
        {
            return knockBackValue;
        }
        set
        {
            knockBackValue = value;
        }
    }

    #endregion Parameters
}
