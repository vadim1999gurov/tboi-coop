﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
sealed public class Walker : MonoBehaviour
{
    #region Constans

    // Character min and max speed stat.
    private const float MinValue = 1f;
    private const float MaxValue = 10f;

    #endregion Constans

    #region State

    public bool CamIsMoving
    {
        get
        {
            return Camera.main.GetComponent<UIMover2D>()?.IsMoving ?? false;
        }
    }

    #endregion State

    #region Parametrs

    [SerializeField]
    [Tooltip("Character speed.")]
    private float value = MinValue;
    /// <summary>
    /// Character speed.
    /// </summary>
    public float Value
    {
        get
        {
            return value;
        }
        set
        {
            if (MinValue <= value && value <= MaxValue) this.value = value;
        }
    }

    #endregion Parametrs

    #region Methods

    private void Move(float speed)
    {
        if (!Input.GetButton("Horizontal") && !Input.GetButton("Vertical")) return;
        Vector3 direct = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direct, Time.deltaTime * speed);
    }

    #endregion Methods

    #region Unity

    private void FixedUpdate()
    {
        if (!CamIsMoving) Move(Value);
    }

    #endregion Unity
}
