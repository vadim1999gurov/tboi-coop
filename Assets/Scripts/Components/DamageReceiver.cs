﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Delegates;
using Expansions;

[DisallowMultipleComponent]
[RequireComponent(typeof(Health))]
sealed public class DamageReceiver : MonoBehaviour
{

    public enum DamageReceiveColor
    {
        red,
        transparent,
        none

    }

    #region Parameters

    [SerializeField]
    [Tooltip("")]
    private float maxTimerImmortality;
    /// <summary>
    /// For comments.
    /// </summary>
    public float MaxTimerImmortality
    {
        get
        {
            return maxTimerImmortality;
        }
        set
        {
            if (value < 0) maxTimerImmortality = 0;
            else maxTimerImmortality = value;
        }
    }

    [SerializeField]
    [Tooltip("")]
    private DamageReceiveColor color;

    [SerializeField]
    [Header("Flag")]
    private bool isKnockedBack = true;

    #endregion Parameters

    #region Flags


    /// <summary>
    /// For comments.
    /// </summary>
    private bool timerImmortalityIsStarted = false;

    #endregion Flags

    #region State

    [System.NonSerialized]
    private Health health;
    /// <summary>
    /// Character health.
    /// </summary>
    public Health Health
    {
        get
        {
            if (health != null) return health;
            return health = GetComponent<Health>();
        }
    }
    [System.NonSerialized]
    private List<Collider2D> colliders;
    /// <summary>
    /// All colliders into this object.
    /// </summary>
    public List<Collider2D> Colliders
    {
        get
        {
            if (colliders != null) return colliders;
            List<Collider2D> collidersTmp = new List<Collider2D>();
            foreach (Collider2D item in GetComponents<Collider2D>()) collidersTmp.Add(item);
            return colliders = collidersTmp;
        }
    }

    [System.NonSerialized]
    private float timerImmortality;
    /// <summary>
    /// For comments.
    /// </summary>
    public float TimerImmortality
    {
        get
        {
            return timerImmortality;
        }
        set
        {
            if (value < 0) timerImmortality = 0;
            else timerImmortality = value;
        }
    }

    Rigidbody2D body;

    #endregion State

    #region Methods

    /// <summary>
    /// For comments.
    /// </summary>
    private void ReceiveDamage(float damage)
    {
        if (!timerImmortalityIsStarted)
        {
            if (Health != null)
            {
                Health.Value -= damage;
                timerImmortality = MaxTimerImmortality;
                timerImmortalityIsStarted = true;
                if (color != DamageReceiveColor.none) StartCoroutine(SetColor());
            }
            else Debug.LogError("Error: health not found!");
        }
    }

    private void Pushing(Vector2 objPosition, float knockPower)
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        if (body != null)
            body.velocity = ((Vector2)transform.position - objPosition).normalized * knockPower + body.velocity;

    }

    private IEnumerator SetColor()
    {
        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();

        Color added = new Color();
        switch (color)
        {
            case DamageReceiveColor.red:
                added = Color.red;
                break;

            case DamageReceiveColor.transparent:
                added = new Color(1, 1, 1, 0);
                break;
        }

        do
        {
            foreach (var el in sprites) el.color = added;
            yield return new WaitForSeconds(0.1f);

            foreach (var el in sprites) el.color = new Color(1, 1, 1, 1);
            yield return new WaitForSeconds(0.1f);
        }
        while (timerImmortalityIsStarted);

        foreach (var el in sprites) el.color = new Color(1, 1, 1, 1);
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (timerImmortalityIsStarted)
        {
            timerImmortality -= Time.deltaTime;
            if (timerImmortality <= 0) timerImmortalityIsStarted = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collider)
    {
        DamageDealer damage = collider.gameObject.GetComponent<DamageDealer>();
        if (damage != null && damage.gameObject.tag != transform.gameObject.tag)
        {
            ReceiveDamage(damage.Value);
            if (isKnockedBack) Pushing(damage.transform.position, damage.KnockBackValue);
        }
    }

    private void FixedUpdate()
    {
        if (body.velocity.sqrMagnitude > 0) body.velocity -= body.velocity.normalized * 0.1f;
        else body.velocity = Vector2.zero;
    }

    #endregion Unity
}
