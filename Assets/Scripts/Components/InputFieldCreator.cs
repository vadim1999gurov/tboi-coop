﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(HorizontalLayoutGroup))]
public class InputFieldCreator : MonoBehaviour
{
    [System.Serializable]
    public class CharImage
    {
        [SerializeField]
        public char letter;
        [SerializeField]
        public Sprite image;

    }

    #region Parameters

    [SerializeField]
    private int maxLength = 10;


    [SerializeField]
    private List<CharImage> alphabet;
    public List<CharImage> Alphabet
    {
        get => alphabet;
    }

    [TextArea(6, 10)]
    [SerializeField]
    private string text;
    public string Text
    {
        get => text;
    }

    #endregion Parameters

    #region State

    private List<Image> word = new List<Image>();

    #endregion State

    #region Methods

    private void InputChar()
    {
        if (text.Length >= maxLength) return;
        if (Input.inputString.Length > 1 || Input.inputString.Length == 0) return;
        CharImage ci;
        try
        {
            ci = Alphabet.Single(el => (el.letter == Input.inputString[0]));
        }
        catch { return; }

        if (ci == null) return;
        else
        {
            text += ci.letter;
            GameObject ngo = new GameObject();
            Image newImage = ngo.AddComponent<Image>();
            ngo.transform.SetParent(transform);
            ngo.transform.localScale = Vector3.one;

            newImage.sprite = ci.image;
            newImage.preserveAspect = true;
            word.Add(newImage);
        }
    }

    private void RemoveChar()
    {
        if (Text.Length > 0)
        {
            text = text.Remove(text.Length - 1);
            Destroy(word[word.Count - 1].gameObject);
            word.Remove(word[word.Count - 1]);
        }
    }

    #endregion Methods

    #region Unity

    private void Update()
    {
        if (Input.anyKey) InputChar();
        if (Input.GetKeyDown(KeyCode.Backspace)) RemoveChar();

    }

    #endregion Unity
}
