﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtifactGiver : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private ArtifactsContainer data;
    public ArtifactsContainer Data
    {
        get => data;
    }

    [SerializeField]
    private Artifact template;

    #endregion Parameters

    #region Methods

    private void DropArtifact()
    {
        Instantiate((template.Data = Data.Next()), transform, false);
    }

    #endregion Methods

    #region Unity

    private void Start()
    {
        DropArtifact();
    }

    #endregion Unity
}
