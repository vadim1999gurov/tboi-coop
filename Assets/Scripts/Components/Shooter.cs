﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[DisallowMultipleComponent]
public sealed class Shooter : MonoBehaviour
{
    /// <summary>
    /// Type for shoot control.
    /// </summary>
    public enum ShootTypeEnum
    {
        Controlling,
        Targeting
    }

    #region Constans
    // Character min and max tears.
    private const int MinRate = 1;
    private const int MaxRate = 10;

    // Character min and max range from shoting.
    private const float MinRange = 1;
    private const int MaxRange = 10;


    // Character min and max damage stat.
    private const float MinDamage = 1f;
    private const float MaxDamage = 10f;


    // Character min and max speed for bullet.
    private const float MinSpeedBullet = 1f;
    private const float MaxSpeedBullet = 10f;

    #endregion Constans

    #region Parameters

    [SerializeField]
    [Tooltip("")]
    private ShootTypeEnum shootType;
    /// <summary>
    ///	
    /// </summary>
    public ShootTypeEnum ShootType
    {
        get
        {
            return shootType;
        }
        set
        {
            shootType = value;
        }
    }

    [SerializeField]
    [Tooltip("")]
    private int rate = MinRate;
    /// <summary>
    ///	
    /// </summary>
    public int Rate
    {
        get
        {
            return rate;
        }
        set
        {
            if (MinRate < value && value < MaxRate) rate = value;
            //else if(value > MaxRate) rate = MaxRate;
            //else if(value < MinRate) rate = MinRate;
        }
    }

    [SerializeField]
    [Tooltip("Character range of shooting.")]
    private float range = MinRange;
    /// <summary>
    /// Character range of shooting.  
    /// </summary>
    public float Range
    {
        get
        {
            return range;
        }
        set
        {
            if (MinRange <= value && value >= MaxRange) range = value;
            //else if(value > MaxRange) range = MaxRange;
            //else if(value < MinRange) range = MinRange;
        }
    }

    [SerializeField]
    [Tooltip("Character damage.")]
    private float damage = MinDamage;
    /// <summary>
    /// Character damage.
    /// </summary>
    public float Damage
    {
        get
        {
            return damage;
        }
        set
        {
            if (MinDamage <= value && value <= MaxDamage) damage = value;
            //else if(value > MaxDamage) damage = MaxDamage;
            //else if(value < MinDamage) damage = MinDamage;
        }
    }


    [SerializeField]
    [Tooltip("Character bullet speed.")]
    private float bulletSpeed = MinSpeedBullet;
    /// <summary>
    /// Character bullet speed.
    /// </summary>
    public float BulletSpeed
    {
        get
        {
            return bulletSpeed;
        }
        set
        {
            if (MinSpeedBullet <= value && value <= MaxSpeedBullet) bulletSpeed = value;
            //else if(value > MaxSpeedBullet) bulletSpeed = MaxSpeedBullet;
            //else if(value < MinSpeedBullet) bulletSpeed = MinSpeedBullet;
        }
    }

    #endregion Parameters

    #region State

    [System.NonSerialized]
    private Tear tearOriginal;
    public Tear TearOriginal
    {
        get
        {
            if (tearOriginal != null) return tearOriginal;
            return tearOriginal = Resources.Load<Tear>("Tears/Prefabs/Tear");
        }
    }

    private bool isShoot = false;
    public bool IsShoot
    {
        get => isShoot;
    }

    #endregion State

    #region Methods

    /// <summary>
    /// Shooting method.
    /// </summary>
    private void Shoot()
    {

        if (!Input.GetButton("ShootHorizontal") && !Input.GetButton("ShootVertical")) return;
        switch (ShootType)
        {
            case ShootTypeEnum.Controlling:
                if (Input.GetButton("ShootHorizontal") ^ Input.GetButton("ShootVertical"))
                {
                    isShoot = true;
                    Vector3 direction = new Vector3((Input.GetAxis("ShootHorizontal")), (Input.GetAxis("ShootVertical")), 0);
                    // Template create.
                    Tear tearTmp = Instantiate(TearOriginal, transform.position + (Vector3.up * 0.87f) + (direction * 0.2f), Quaternion.identity);
                    tearTmp.DamageDealer.Value = Damage;
                    tearTmp.gameObject.tag = gameObject.tag;
                    tearTmp.Speed = BulletSpeed;
                    tearTmp.Target = transform.position + (Vector3.up * 0.87f) + direction * Range;
                    tearTmp.NameCreatorObject = transform.name;

                    Collider2D collider = tearTmp.GetComponent<Collider2D>();
                    foreach (var el in GetComponentsInChildren<Collider2D>().Where(el => !el.isTrigger)) Physics2D.IgnoreCollision(collider, el);
                }
                break;

            case ShootTypeEnum.Targeting:
                // TO DO.
                break;
        }
        StartTimer();
    }

    private void StartTimer()
    {
        coolDownTimer = Rate * 0.2f;
    }

    #endregion Methods

    #region State

    private float coolDownTimer;
    public float CoolDownTimer
    {
        get
        {
            return coolDownTimer;
        }
    }

    #endregion State


    #region Unity

    private void Update()
    {
        if (coolDownTimer <= 0)
        {
            isShoot = false;
            Shoot();
        }
        else
        {
            coolDownTimer -= Time.deltaTime;
        }
    }

    #endregion Unity
}
