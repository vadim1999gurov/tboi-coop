﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rewarder : MonoBehaviour
{
    #region Enum

    private enum Config
    {
        Awake,
        Start,
        CallOnly
    }

    #endregion Enum

    #region Parameters

    [SerializeField]
    private GameObjectsContainer container;
    public GameObjectsContainer Container
    {
        get => container;
    }

    [SerializeField]
    private Config callConfig = Config.CallOnly;

    #endregion Parameters

    #region Methods

    public GameObject DropReward(Transform parent = null)
    {
        if (Random.Range(0, 2) == 1) return null;
        return Instantiate(Container.Next(), parent ?? transform, false);
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        if (callConfig == Config.Awake) DropReward();
    }

    private void Start()
    {
        if (callConfig == Config.Start) DropReward();
    }

    #endregion Unity
}