﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Expansion;

public enum MovementType
{
    linear,
    sigmoidal,
    circle
}

public class Stalker : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private MovementType moveType = MovementType.linear;

    [SerializeField]
    private float agility = 1;

    [SerializeField]
    private float speed = 3;

    #endregion Parameters

    #region State

    private GameObject target;

    private Vector2 direction;


    #endregion State

    #region Methods

    private IEnumerator ChangeDirection(float time)
    {
        yield return new WaitForSeconds(time);
        if (target != null) direction = (target.transform.position - transform.position).normalized;
        StartCoroutine(ChangeDirection(time));
    }

    private void Move()
    {
        if (target != null)
        {
            switch (moveType)
            {
                case MovementType.linear:
                    LinearMove();
                    break;

                case MovementType.sigmoidal:
                    SigmoidalMove();
                    break;
                case MovementType.circle:
                    CircleMove();
                    break;
            }
        }
        else
        {

        }
    }

    private void LinearMove()
    {
        direction = (target.transform.position - transform.position).normalized;
        transform.position = Vector2.MoveTowards(transform.position, ((Vector2)(transform.position)).StepTo(direction), speed * Time.fixedDeltaTime);
    }

    private void CircleMove()
    {
        float angle = (Vector2.SignedAngle(direction.normalized, target.transform.position - transform.position) > 0 ? 1 : -1);
        direction = (target.transform.position - transform.position).normalized;

        transform.position = Vector2.MoveTowards(transform.position, ((Vector2)(transform.position)).StepTo(direction).Rotate(angle * 0.01f), speed * Time.fixedDeltaTime);
    }

    private void SigmoidalMove()
    {

    }

    #endregion Methods

    #region Untiy

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (target != null) return;
        target = collider.GetComponent<Character>()?.gameObject;
        StartCoroutine(ChangeDirection(0.2f));
    }


    private void FixedUpdate()
    {
        Move();
    }

    #endregion Unity
}