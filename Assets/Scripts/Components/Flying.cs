﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Flying : MonoBehaviour
{
    #region Unity

    private void Awake()
    {
        gameObject.layer = LayerMask.NameToLayer("Flying");
    }

    #endregion Unity
}
