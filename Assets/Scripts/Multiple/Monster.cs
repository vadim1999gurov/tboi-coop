﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interfaces;
using Delegates;

[DisallowMultipleComponent]
[RequireComponent(typeof(DamageDealer))]
[RequireComponent(typeof(DamageReceiver))]
[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Stalker))]

sealed public class Monster : MonoBehaviour, ILifeObject
{
    #region Events

    public event Call OnDeath;

    #endregion Events

    #region State

    [System.NonSerialized]
    private DamageReceiver damageReciver;
    /// <summary>
    ///	Link to DamageReceiver component.
    /// </summary>
    public DamageReceiver DamageReciver
    {
        get
        {
            if (damageReciver != null) return damageReciver;
            return damageReciver = GetComponent<DamageReceiver>();
        }
    }

    private DamageDealer damageDealer;
    /// <summary>
    ///	Link to DamageDealer component.
    /// </summary>
    public DamageDealer DamageDealer
    {
        get
        {
            if (damageDealer != null) return damageDealer;
            return damageDealer = GetComponent<DamageDealer>();
        }
    }

    [System.NonSerialized]
    private Health health;
    /// <summary>
    ///	Link to Health component.
    /// </summary>
    public Health Health
    {
        get
        {
            if (health != null) return health;
            return health = GetComponent<Health>();
        }
    }

    #endregion State

    public void Die(Call Call = null)
    {
        if (OnDeath != null) OnDeath.Invoke();
        Destroy(this.gameObject);
    }
}
