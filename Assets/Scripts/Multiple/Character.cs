﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Interfaces;
using Delegates;

[DisallowMultipleComponent]
[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Walker))]
[RequireComponent(typeof(Shooter))]
[RequireComponent(typeof(DamageReceiver))]
sealed public class Character : MonoBehaviour, ILifeObject
{
    #region State

    [System.NonSerialized]
    private Shooter shooter;
    /// <summary>
    ///	Link to Shooter component.
    /// </summary>
    public Shooter Shooter
    {
        get
        {
            if (shooter != null) return shooter;
            return shooter = GetComponent<Shooter>();
        }
    }

    [System.NonSerialized]
    private Walker walker;
    /// <summary>
    ///	Link to Walker component.
    /// </summary>
    public Walker Walker
    {
        get
        {
            if (walker != null) return walker;
            return walker = GetComponent<Walker>();
        }
    }

    [System.NonSerialized]
    private Health health;
    /// <summary>
    ///	Link to Health component.
    /// </summary>
    public Health Health
    {
        get
        {
            if (health != null) return health;
            return health = GetComponent<Health>();
        }
    }

    [System.NonSerialized]
    private Stats allStats = new Stats();
    /// <summary>
    ///	Character characteristics.
    /// </summary>
    public Stats AllStats
    {
        get
        {
            return allStats;
        }
        set
        {
            allStats = value;
            Health.Value = allStats.Health;
            Walker.Value = allStats.Speed;
            Shooter.Damage = allStats.Damage;
            Shooter.Rate = allStats.Rate;
            Shooter.Range = allStats.Range;
        }
    }

    private Animator animator;

    private enum StateAnimation
    {
        Back,
        Horizontal,
        Forward,
        Idly
    }

    private StateAnimation HeadAnimation
    {
        get => (StateAnimation)animator.GetInteger("StateHead");
        set => animator.SetInteger("StateHead", (int)value);
    }

    private StateAnimation BodyAnimation
    {
        get => (StateAnimation)animator.GetInteger("StateBody");
        set => animator.SetInteger("StateBody", (int)value);
    }

    private GameObject head;
    public GameObject Head
    {
        get => head ?? (head = GetComponentsInChildren<Transform>().Single(el => el.name == "Head").gameObject);
    }

    private GameObject body;
    public GameObject Body
    {
        get => body ?? (body = GetComponentsInChildren<Transform>().Single(el => el.name == "Body").gameObject);
    }

    #endregion State

    #region Methods

    public void Die(Call Call = null)
    {
        Destroy(this.gameObject);
    }

    private void LoadStats()
    {
        allStats.Health = Health.Value;
        allStats.Speed = Walker.Value;
        allStats.Damage = Shooter.Damage;
        allStats.Rate = Shooter.Rate;
        allStats.Range = Shooter.Range;

    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        LoadStats();
    }

    private void FixedUpdate()
    {
        if (Input.GetButton("Vertical"))
        {
            BodyAnimation = StateAnimation.Forward;
            if (!Shooter.IsShoot)
            {
                if (Input.GetAxis("Vertical") < 0) HeadAnimation = StateAnimation.Forward;
                else HeadAnimation = StateAnimation.Back;
            }
        }
        else if (Input.GetButton("Horizontal"))
        {

            BodyAnimation = StateAnimation.Horizontal;
            Body.GetComponent<SpriteRenderer>().flipX = Input.GetAxis("Horizontal") < 0;

            if (!Shooter.IsShoot)
            {
                HeadAnimation = StateAnimation.Horizontal;
                Head.GetComponent<SpriteRenderer>().flipX = Input.GetAxis("Horizontal") < 0;
            }
        }
        else
        {
            BodyAnimation = StateAnimation.Idly;
            if (!Shooter.IsShoot) HeadAnimation = StateAnimation.Idly;
        }

        animator.SetBool("isShoot", Shooter.IsShoot);
        if (Shooter.IsShoot)
        {
            animator.SetFloat("Time", Shooter.CoolDownTimer);
            if (Input.GetButton("ShootVertical"))
            {
                if (Input.GetAxis("ShootVertical") < 0) HeadAnimation = StateAnimation.Forward;
                else HeadAnimation = StateAnimation.Back;
            }
            else if (Input.GetButton("ShootHorizontal"))
            {
                HeadAnimation = StateAnimation.Horizontal;
                Head.GetComponent<SpriteRenderer>().flipX = Input.GetAxis("ShootHorizontal") < 0;
            }
        }
    }

    #endregion Unity
}
