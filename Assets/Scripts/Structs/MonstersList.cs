﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Monsters", menuName = "Containers/Monsters", order = 2)]
public class MonstersList : ScriptableObject
{
    #region Parameters

    [SerializeField]
    private List<Monster> all = new List<Monster>();
    public List<Monster> All
    {
        get => all;
        set => all = value;
    }

    #endregion Parameters

    #region Methods

    public Monster Next()
    {
        return all?[Random.Range(0, all.Count)];
    }

    #endregion Methods

}
