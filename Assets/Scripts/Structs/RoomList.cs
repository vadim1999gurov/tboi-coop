﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[CreateAssetMenu(fileName = "RoomList", menuName = "Structs/RoomList", order = 0)]
/// <summary>
/// Структура для хранения списка комнат, относящихся к опредленному типу этажей.
/// </summary>
public class RoomList : ScriptableObject
{
    #region Parameters

    [SerializeField]
    [Tooltip("Поле, которое будет хранить начальную комнату, для текущего типа этажей.")]
    private Room beginRoom;
    public Room BeginRoom
    {
        get
        {
            return beginRoom;
        }
    }

    [SerializeField]
    private List<Room> rooms;

    #endregion Parameters

    #region Methods

    public Room Next()
    {
        return rooms[Random.Range(0, rooms.Count)];
    }

    #endregion Methods
}
