﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[CreateAssetMenu(fileName = "RoomContainer", menuName = "Containers/RoomContainer", order = 1)]
public class RoomListContainer : ScriptableObject
{
    [SerializeField]
    private List<RoomList> all;
    public List<RoomList> All
    {
        get
        {
            return all;
        }
    }

    public RoomList this[int index]
    {
        get
        {
            return All[index];
        }
        set
        {
            All[index] = value;
        }
    }

}
