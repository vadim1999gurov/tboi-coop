﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Container", menuName = "Containers/ArtifactsContainer", order = 4)]
public class ArtifactsContainer : ScriptableObject
{
    #region Parameters

    [SerializeField]
    private List<ArtifactData> all;
    public List<ArtifactData> All
    {
        get
        {
            return all;
        }
    }

    #endregion Parameters

    #region Methods

    public ArtifactData Next()
    {
        return All[Random.Range(0, All.Count)];
    }

    #endregion Methods

    #region Iterator
    public ArtifactData this[int index]
    {
        get
        {
            return All[index];
        }
        set
        {
            All[index] = value;
        }
    }

    #endregion Iterator
}
