﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
sealed public class Stats 
{
	#region Parameters
	
	[SerializeField]
    [Tooltip("")]
	private float health;
	/// <summary>
    ///	
    /// </summary>
	public float Health
	{
		get
		{
			return health;
		}
		set
		{
			health = value;
		}
	}

	[SerializeField]
    [Tooltip("")]
	private float speed;
	/// <summary>
    ///	
    /// </summary>
	public float Speed
	{
		get
		{
			return speed;
		}
		set
		{
			speed = value;
		}
	}

	[SerializeField]
    [Tooltip("")]
	private float damage;
	/// <summary>
    ///	
    /// </summary>
	public float Damage
	{
		get
		{
			return damage;
		}
		set
		{
			damage = value;
		}
	}


	[SerializeField]
    [Tooltip("")]
	private int rate;
	/// <summary>
    ///	
    /// </summary>
	public int Rate
	{
		get
		{
			return rate;
		}
		set
		{
			rate = value;
		}
	}


	[SerializeField]
    [Tooltip("")]
	private float range;
	/// <summary>
    ///	
    /// </summary>
	public float Range
	{
		get
		{
			return range;
		}
		set
		{
			range = value;
		}
	}
	#endregion

	#region Operators

	/// <summary>
    /// Overload operator "+".
    /// </summary>
	public static Stats operator +(Stats s1, Stats s2)
	{
		Stats stats = new Stats();
		Debug.Log($"s1 {s1.Health}  s2 {s2.Health}");
		stats.Health = s1.Health + s2.Health;
		stats.Speed = s1.Speed + s2.Speed;
		stats.Damage = s1.Damage + s2.Damage;
		stats.Rate = s1.Rate + s2.Rate;
		stats.Range = s1.Range + s2.Range;
		return stats;
	}

	/// <summary>
    /// Overload operator "-".
    /// </summary>
	public static Stats operator -(Stats s1, Stats s2)
	{
		Stats stats = new Stats();
		stats.Health = s1.Health - s2.Health;
		stats.Speed = s1.Speed - s2.Speed;
		stats.Damage = s1.Damage - s2.Damage;
		stats.Rate = s1.Rate - s2.Rate;
		stats.Range = s1.Range - s2.Range;
		return stats;
	}

	#endregion

}
