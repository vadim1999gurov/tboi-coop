﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Delegates;

[System.Serializable]
[CreateAssetMenu(fileName = "Item", menuName = "Structs/Item", order = 0)]
sealed public class ItemData : ScriptableObject
{

    #region Parameters

    [SerializeField]
    [Tooltip("Item icon.")]
    private Sprite icon;
    /// <summary>
    ///	Item icon.
    /// </summary>
    public Sprite Icon
    {
        get
        {
            return icon;
        }
        set
        {
            icon = value;
        }
    }

    [SerializeField]
    [Tooltip("Item name.")]
    private new string name;
    /// <summary>
    ///	Item name.
    /// </summary>
    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }

    [SerializeField]
    [Tooltip("Count coins.")]
    private float coins = 0;
    /// <summary>
    ///	Count coins.
    /// </summary>
    public float Coins
    {
        get
        {
            return coins;
        }
        set
        {
            if (value >= 0) coins = value;
        }
    }


    [SerializeField]
    [Tooltip("Count bombs.")]
    private float bombs = 0;
    /// <summary>
    ///	Count bombs.
    /// </summary>
    public float Bombs
    {
        get
        {
            return bombs;
        }
        set
        {
            if (value >= 0) bombs = value;
        }
    }


    [SerializeField]
    [Tooltip("Count keys.")]
    private float keys = 0;
    /// <summary>
    ///	Count keys.
    /// </summary>
    public float Keys
    {
        get
        {
            return keys;
        }
        set
        {
            if (value >= 0) keys = value;
        }
    }

    #endregion Parameters

    #region Methods

    public override string ToString()
    {
        return $"Sprite: {Icon?.name}, Name: {Name}| Coins: {Coins}| Bombs: {Bombs}| Keys: {Keys}";
    }

    #endregion Methods

    #region Overload

    /// <summary>
    /// Overload operator "+".
    /// </summary>
    public static ItemData operator +(ItemData c1, ItemData c2)
    {
        ItemData con = ScriptableObject.CreateInstance<ItemData>();
        con.bombs = c1.Bombs + c2.Bombs;
        con.coins = c1.Coins + c2.Coins;
        con.keys = c1.Keys + c2.Keys;

        return con;
    }

    /// <summary>
    /// Overload operator "-".
    /// </summary>
    public static ItemData operator -(ItemData c1, ItemData c2)
    {
        ItemData con = ScriptableObject.CreateInstance<ItemData>();
        con.Bombs = c1.Bombs - c2.Bombs;
        con.Coins = c1.Coins - c2.Coins;
        con.Keys = c1.Keys - c2.Keys;
        return con;
    }
    #endregion

}
