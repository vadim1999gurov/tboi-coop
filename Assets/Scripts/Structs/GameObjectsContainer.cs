﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Container", menuName = "Containers/GameObjectsContainer", order = 3)]
public class GameObjectsContainer : ScriptableObject
{
    #region State

    [SerializeField]
    private List<GameObject> all;
    public List<GameObject> All
    {
        get
        {
            return all;
        }
    }

    #endregion State

    #region Methods

    public GameObject Next()
    {
        return All[Random.Range(0, All.Count)];
    }

    #endregion Methods

    #region Iterator
    public GameObject this[int index]
    {
        get
        {
            return All[index];
        }
        set
        {
            All[index] = value;
        }
    }

    #endregion Iterator

}
