﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Delegates;

namespace Interfaces
{
    public interface ILifeObject
    {
        void Die(Call Call = null);
    }
}