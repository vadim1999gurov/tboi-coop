﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapCell : MonoBehaviour
{
    #region Enum

    public enum State
    {
        Hidden,
        Detected,
        IsUsed
    }

    #endregion Enum

    #region Parameters

    public Image cellImage;
    public Image icon;

    public Sprite detectedImage;
    public Sprite isUsedImage;


    [SerializeField]
    private State cellState;
    public State CellState
    {
        get => cellState;
        set
        {
            cellState = value;
            switch (value)
            {
                case State.Hidden:
                    icon.enabled = cellImage.enabled = false;
                    break;
                case State.Detected:
                    icon.enabled = cellImage.enabled = true;
                    cellImage.sprite = detectedImage;
                    break;
                case State.IsUsed:
                    icon.enabled = cellImage.enabled = true;
                    cellImage.sprite = isUsedImage;
                    break;
            }
            if (icon.sprite == null) icon.enabled = false;
        }
    }

    #endregion Parameters

    #region Unity

#if UNITY_EDITOR
    private void OnValidate()
    {
        CellState = cellState;
    }
#endif

    #endregion Unity

}
