﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMoverOnTime2D : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    [Header("Positions")]
    private Vector2 beginPosition;
    public Vector2 BeginPosition
    {
        get
        {
            return beginPosition;
        }
        set
        {
            beginPosition = value;
        }
    }

    [SerializeField]
    private Vector2 endPosition;
    public Vector2 EndPosition
    {
        get
        {
            return endPosition;
        }
        set
        {
            endPosition = value;
        }
    }

    [Header("Time")]
    [SerializeField]
    private float timeAnimation = 1;
    public float TimeAnimation
    {
        get
        {
            return timeAnimation;
        }
        set
        {
            timeAnimation = value;
        }
    }

    [SerializeField]
    private float timeReverseAnimation = 1;
    public float TimeReverseAnimation
    {
        get
        {
            return timeReverseAnimation;
        }
        set
        {
            timeReverseAnimation = value;
        }
    }

    [SerializeField]
    [Header("Optional")]
    private bool currentPositionIsBegin = false;

    #endregion Parameters

    #region State

    private bool isMoving = false;
    private bool IsMoving
    {
        get
        {
            return isMoving;
        }
    }
    private float timer = 0;

    private Coroutine currentMoving;

    #endregion State

    #region Methods

    public void StartAnimation()
    {

        if (currentMoving != null) StopCoroutine(currentMoving);
        if (timer < 0 || !isMoving) timer = 0;
        if (gameObject.activeInHierarchy) currentMoving = StartCoroutine(Move(false));
    }

    public void StartReverseAnimation()
    {
        if (currentMoving != null) StopCoroutine(currentMoving);
        if (timer > TimeReverseAnimation || !isMoving) timer = TimeReverseAnimation;
        currentMoving = StartCoroutine(Move(true));
    }

    private IEnumerator Move(bool isReverse)
    {

        float AllTime = (isReverse) ? TimeReverseAnimation : TimeAnimation;
        isMoving = true;
        do
        {
            if (isReverse) timer -= Time.deltaTime;
            else timer += Time.deltaTime;

            if (GetComponent<RectTransform>() != null) GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(BeginPosition, EndPosition, timer / AllTime);
            else transform.localPosition = Vector2.Lerp(BeginPosition, EndPosition, timer / AllTime);

            yield return null;
        }
        while ((!isReverse && (EndPosition != (Vector2)transform.localPosition)) || (isReverse && (BeginPosition != (Vector2)transform.localPosition)));
        isMoving = false;
        currentMoving = null;
    }

    #endregion Methods


    #region Unity

    private void Awake()
    {
        if (currentPositionIsBegin) beginPosition = transform.localPosition;
    }

    private void OnValidate()
    {
        if (currentPositionIsBegin) beginPosition = transform.localPosition;
    }

    #endregion Unity
}
