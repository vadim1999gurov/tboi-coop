﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Delegates;

public class UIMover2D : MonoBehaviour
{
    #region Events

    public event Call OnStop;

    #endregion Events

    #region Parameters

    [SerializeField]
    [Header("Positions")]
    private Vector2 beginPosition;
    public Vector2 BeginPosition
    {
        get
        {
            return beginPosition;
        }
        set
        {
            beginPosition = value;
        }
    }

    [SerializeField]
    private Vector2 endPosition;
    public Vector2 EndPosition
    {
        get
        {
            return endPosition;
        }
        set
        {
            endPosition = value;
        }
    }

    [Header("Speed")]
    [SerializeField]
    private float minSpeed = 1;

    [SerializeField]
    private float maxSpeed = 3;


    [SerializeField]
    [Header("Optional")]
    private bool currentPositionIsBegin = false;

    [SerializeField]
    private float accuracy = 1;

    #endregion Parameters

    #region State

    private bool isMoving = false;
    public bool IsMoving
    {
        get
        {
            return isMoving;
        }
    }

    private Coroutine currentMoving;

    #endregion State

    #region Methods

    public void StartAnimation(Vector3? targetPosition = null, Call Call = null)
    {
        if (targetPosition != null) endPosition = (Vector3)targetPosition;
        if (currentMoving != null) StopCoroutine(currentMoving);
        currentMoving = StartCoroutine(Move(Call));
    }

    public void StartReverseAnimation(Vector3? targetPosition = null, Call Call = null)
    {
        if (targetPosition != null) endPosition = (Vector3)targetPosition;
        if (currentMoving != null) StopCoroutine(currentMoving);
        currentMoving = StartCoroutine(Move(Call));
    }


    private IEnumerator Move(Call Call = null)
    {
        if (currentPositionIsBegin) beginPosition = transform.localPosition;

        isMoving = true;
        do
        {
            if ((EndPosition - (Vector2)Camera.main.transform.position).sqrMagnitude < accuracy) Camera.main.transform.position = new Vector3(EndPosition.x, EndPosition.y, Camera.main.transform.position.z);
            else
            {
                float allLength = (EndPosition - beginPosition).sqrMagnitude;
                float leftLength = (EndPosition - (Vector2)Camera.main.transform.position).sqrMagnitude;


                float speed;
                if (leftLength < (allLength / 2)) speed = Mathf.Lerp(minSpeed, maxSpeed, (leftLength / (allLength)));
                else speed = Mathf.Lerp(maxSpeed, minSpeed, (leftLength / allLength));

                Vector2 step = (EndPosition - (Vector2)Camera.main.transform.position).normalized * speed;
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x + step.x, Camera.main.transform.position.y + step.y, Camera.main.transform.position.z);
            }
            yield return null;
        }
        while ((Vector2)Camera.main.transform.position != EndPosition);
        if (Call != null) Call();
        if (OnStop != null) OnStop.Invoke();
        isMoving = false;
    }

    #endregion Methods


    #region Unity

    private void Awake()
    {
        StartAnimation();
    }

    private void OnValidate()
    {
        if (currentPositionIsBegin) beginPosition = transform.localPosition;
    }

    #endregion Unity
}
