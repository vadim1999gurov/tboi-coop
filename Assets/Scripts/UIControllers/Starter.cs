﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Starter : MonoBehaviour
{

    private void StartGame()
    {
        if (!StateController.Instance.camIsMooving)
            SceneManager.LoadScene("GamePlay", LoadSceneMode.Single);
    }

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(StartGame);
    }
}
