﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleSkip : MonoBehaviour
{

    void Update()
    {
        if (Input.anyKeyDown && !StateController.Instance.camIsMooving) StateController.Instance.ChangeState("Menu");
    }
}
