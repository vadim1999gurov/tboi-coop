﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exiter : MonoBehaviour
{
    #region Methods

    private void Exit()
    {
        if (!StateController.Instance.camIsMooving)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
		    Application.Quit();
#endif
        }
    }

    #endregion Methods


    #region Unity

    private void Awake()
    {
        Button tmpButton = GetComponent<Button>();
        if (tmpButton != null) tmpButton.onClick.AddListener(Exit);
    }

    #endregion Unity
}
