﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private string stateName;
    public string StateName
    {
        get
        {
            return stateName;
        }
        set
        {
            stateName = value;
        }
    }


    [SerializeField]
    private Vector2 archoredPosition;
    public Vector2 ArchoredPosition
    {
        get
        {
            if (useCurrentPosition)
            {
                if (archoredPosition == GetComponent<RectTransform>().anchoredPosition) return archoredPosition;
                else return archoredPosition = GetComponent<RectTransform>().anchoredPosition;
            }
            return archoredPosition;
        }
        set
        {
            archoredPosition = value;
        }
    }

    #endregion Parameters

    #region Unity

    private void Update()
    {
        if (!StateController.Instance.camIsMooving)
        {
            if (Input.GetKey(KeyCode.Escape)) StateController.Instance.ChangeState(StateName);
        }
    }

    #endregion Unity

    [Header("Optional")]
    [SerializeField]
    private bool useCurrentPosition;


}
