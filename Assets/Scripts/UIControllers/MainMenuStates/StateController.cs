﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StateController : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private string fistState;
    public string FistState
    {
        get
        {
            return fistState;
        }
    }

    [Header("Camera")]
    [SerializeField]
    private float minSpeed = 1;

    [SerializeField]
    private float maxSpeed = 3;


    #endregion Parameters


    #region State

    [System.NonSerialized]
    public static StateController Instance = null;

    enum CameraState
    {
        Idly,
        Moving
    }
    [Header("Debug")]
    [SerializeField]
    public bool camIsMooving;
    private State currentState, prevState;
    private Coroutine moving;
    private Dictionary<string, State> states = new Dictionary<string, State>();

    private Vector2 beginPosition;

    #endregion State

    #region Methods

    public void ChangeState(string order)
    {
        if (moving != null) StopCoroutine(moving);

        if (currentState != null) prevState = currentState;
        else if (states.Count > 0) prevState = states.First().Value;

        currentState = states[order];
        beginPosition = Camera.main.transform.position;
        moving = StartCoroutine(MoveCam());
    }

    private IEnumerator MoveCam()
    {
        camIsMooving = true;
        currentState.gameObject.SetActive(true);
        do
        {
            if ((currentState.ArchoredPosition - (Vector2)Camera.main.transform.position).magnitude < 1) Camera.main.transform.position = currentState.ArchoredPosition;
            else
            {
                float allLength = (beginPosition - currentState.ArchoredPosition).magnitude;
                float leftLength = (currentState.ArchoredPosition - (Vector2)Camera.main.transform.position).magnitude;

                float speed;
                if (leftLength < (allLength / 2)) speed = Mathf.Lerp(minSpeed, maxSpeed, (leftLength / allLength));
                else speed = Mathf.Lerp(maxSpeed, minSpeed, (leftLength / allLength));

                Vector2 step = (currentState.ArchoredPosition - (Vector2)Camera.main.transform.position).normalized * speed;

                Camera.main.transform.position = new Vector2(Camera.main.transform.position.x + step.x, Camera.main.transform.position.y + step.y);
            }
            yield return null;
        }
        while ((Vector2)Camera.main.transform.position != currentState.ArchoredPosition);

        moving = null;
        camIsMooving = false;

        if (currentState != prevState) prevState.gameObject.SetActive(false);
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        if (Instance == null) Instance = this;
        foreach (var el in GetComponentsInChildren<State>()) states[el.StateName] = el;
    }

    private void Start()
    {
        ChangeState(FistState);
    }

    #endregion Unity
}
