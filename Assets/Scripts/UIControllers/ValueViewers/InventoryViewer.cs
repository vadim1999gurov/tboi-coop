﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryViewer : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private Inventory targetData;
    public Inventory TargetData
    {
        get => targetData;
        set => targetData = value;
    }

    #endregion Parameters

    #region State

    private TextMeshProUGUI coins;
    public TextMeshProUGUI Coins
    {
        get => coins;
        set => coins = value;
    }

    private TextMeshProUGUI bombs;
    public TextMeshProUGUI Bombs
    {
        get => bombs;
        set => bombs = value;
    }

    private TextMeshProUGUI keys;
    public TextMeshProUGUI Keys
    {
        get => keys;
        set => keys = value;
    }

    #endregion State

    #region Methods

    private void UpdateValues()
    {
        if (targetData != null && targetData?.Resources != null)
        {
            Coins.text = $"x{targetData.Resources.Coins}";
            Bombs.text = $"x{targetData.Resources.Bombs}";
            Keys.text = $"x{targetData.Resources.Keys}";
        }
        else Debug.LogError($"Error [{this.ToString()}]: Data is null");
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        if (TargetData != null)
        {
            foreach (var el in GetComponentsInChildren<Transform>())
            {
                if (el.name == "Coins") Coins = el.GetComponentInChildren<TextMeshProUGUI>();
                if (el.name == "Bombs") Bombs = el.GetComponentInChildren<TextMeshProUGUI>();
                if (el.name == "Keys") Keys = el.GetComponentInChildren<TextMeshProUGUI>();

            }
            if (Coins == null || Bombs == null && Keys == null) Debug.LogError($"Error [{this.ToString()}]: Not all components did be found");

            if (TargetData.Resources != null) targetData.OnEditData += UpdateValues;
            else Debug.LogError($"Error [{this.ToString()}]:Resources in targetData is null");

            UpdateValues();
        }
        else Debug.LogError($"Error [{this.ToString()}]: Data is null");
    }

    #endregion Unity

}
