﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{
    #region Singleton

    public static MiniMap Instance;

    #endregion Singleton

    #region Parameters

    [SerializeField]
    private MapCell cellTemplate;

    [SerializeField]
    private Sprite shopIcn;

    [SerializeField]
    private Sprite treasuryIcn;

    [SerializeField]
    private Sprite bossIcn;

    #endregion Parameters

    #region State
    private MapCell[,] map;
    public MapCell[,] Map
    {
        get => map;
        set => map = value;
    }

    private GameObject cellsParent;

    private Vector2 curPos;

    #endregion State

    #region Methods

    private void Generate()
    {
        Map = new MapCell[FloorGeneration.Grid.XMax, FloorGeneration.Grid.YMax];
        FloorGeneration.Grid grid = FloorGeneration.Instance.floor;

        for (int x = 0; x < FloorGeneration.Grid.XMax; x++)
        {
            for (int y = 0; y < FloorGeneration.Grid.YMax; y++)
            {
                if (grid[x, y]?.IsReserved != null)
                {
                    MapCell newCell = Map[x, y] = Instantiate(cellTemplate, cellsParent.transform, false);

                    float xTmp = FloorGeneration.Grid.ArrayToMapX(x) * cellTemplate.GetComponent<RectTransform>().rect.width,
                    yTmp = FloorGeneration.Grid.ArrayToMapY(y) * cellTemplate.GetComponent<RectTransform>().rect.height;

                    newCell.transform.localPosition = new Vector3(xTmp, yTmp, 0);

                    newCell.CellState = MapCell.State.Hidden;

                    switch (grid[x, y].Room.Type)
                    {
                        case TypeRoom.Treasury:
                            if (treasuryIcn != null) newCell.icon.sprite = treasuryIcn;
                            break;
                        case TypeRoom.Shop:
                            if (shopIcn != null) newCell.icon.sprite = shopIcn;
                            break;
                        case TypeRoom.Boss:
                            if (bossIcn != null) newCell.icon.sprite = bossIcn;
                            break;
                    }
                }
            }
        }

        Map[FloorGeneration.Grid.XMax / 2, FloorGeneration.Grid.YMax / 2].CellState = MapCell.State.IsUsed;
    }

    public void MoveTo(string direction)
    {
        bool flag = false;
        Map[FloorGeneration.Grid.MapToArrayX((int)curPos.x), FloorGeneration.Grid.MapToArrayY((int)curPos.y)].CellState = MapCell.State.Detected;
        switch (direction)
        {
            case "Left":
                curPos.x--;
                flag = true;
                break;

            case "Right":
                curPos.x++;
                flag = true;
                break;

            case "Up":
                curPos.y++;
                flag = true;
                break;

            case "Down":
                curPos.y--;
                flag = true;
                break;
        }

        if (flag)
        {
            Map[FloorGeneration.Grid.MapToArrayX((int)curPos.x), FloorGeneration.Grid.MapToArrayY((int)curPos.y)].CellState = MapCell.State.IsUsed;
        }
        cellsParent.transform.localPosition = -Map[FloorGeneration.Grid.MapToArrayX((int)curPos.x), FloorGeneration.Grid.MapToArrayY((int)curPos.y)].transform.localPosition;
    }

    public void OpenCellsMap()
    {
        FloorGeneration.Grid grid = FloorGeneration.Instance.floor;
        for (int x = 0; x < FloorGeneration.Grid.XMax; x++)
        {
            for (int y = 0; y < FloorGeneration.Grid.YMax; y++)
            {
                if (grid[x, y]?.IsReserved != null && !grid[x, y].Room.IsUsed)
                {
                    Map[x, y].CellState = MapCell.State.Detected;
                }
            }
        }
    }

    public void OpenIconMap()
    {
        FloorGeneration.Grid grid = FloorGeneration.Instance.floor;
        for (int x = 0; x < FloorGeneration.Grid.XMax; x++)
        {
            for (int y = 0; y < FloorGeneration.Grid.YMax; y++)
            {
                if (grid[x, y]?.IsReserved != null && !grid[x, y].Room.IsUsed)
                {
                    if (Map[x, y].icon != null && Map[x, y].icon.sprite != null) Map[x, y].icon.enabled = true;
                }
            }
        }
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        if (Instance == null) Instance = this;
        foreach (Transform el in GetComponentsInChildren<Transform>())
        {
            if (el.name == "Cells") cellsParent = el.gameObject;
        }
    }

    private void Start()
    {
        Generate();
    }

    #endregion Unity
}
