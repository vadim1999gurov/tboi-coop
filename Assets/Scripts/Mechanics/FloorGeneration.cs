﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Delegates;

[DisallowMultipleComponent]
public class FloorGeneration : MonoBehaviour
{
    #region Singleton

    private static FloorGeneration instance;
    public static FloorGeneration Instance
    {
        get
        {
            return instance;
        }
    }

    #endregion Singleton

    #region Events

    public event Call onEndGenerate;

    #endregion Events

    public RoomListContainer rooms;

    #region State

    [System.NonSerialized]
    private GameObject roomsParent;
    public GameObject RoomsParent
    {
        get
        {
            if (roomsParent != null) return roomsParent;
            return GetComponentsInChildren<Transform>(true).Single(el => el.name == "Rooms").gameObject;
        }
    }

    static int level = 0;

    public Grid floor = new Grid();

    #endregion State

    /// <summary>
    ///
    /// </summary>
    private struct Marker
    {
        private int x;
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                if (Grid.XMax > value) x = value;
            }
        }

        private int y;
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                if (Grid.YMax > value) y = value;
            }
        }

        public Marker(int x = 0, int y = 0)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2 ToVector2()
        {
            return new Vector2(this.X, this.Y);
        }

        public Vector2 ToVector3()
        {
            return new Vector3(this.X, this.Y, 0);
        }

        public new string ToString()
        {
            return $"({this.X}, {this.Y})";
        }

        public static bool operator ==(Marker m1, Marker m2)
        {
            return (m1.X == m2.X && m1.Y == m2.Y);
        }

        public static bool operator !=(Marker m1, Marker m2)
        {
            return (m1.X != m2.X && m1.Y != m2.Y);
        }
    }



    /// <summary>
    ///
    /// </summary>
    public class Cell
    {
        private bool isReserved = false;
        public bool IsReserved
        {
            get
            {
                return isReserved;
            }
        }

        private Room room = new Room();
        public Room Room
        {
            get
            {
                return room;
            }
            set
            {
                room = value;
                isReserved = (room != null);
            }
        }

        public Cell(Room inpRoom)
        {
            this.Room = inpRoom;
        }
    }

    #region Methods

    /// <summary>
    ///
    /// </summary>
    private void GenerateFloor()
    {
        // Инициализация хранилища для этажа.
        floor = new Grid();

        // Инкеремнитруем номер этажа.


        // Создание центральной комнаты (начальная точка).
        Cell begin = floor[Grid.XMax / 2, Grid.YMax / 2] = new Cell(CreateRoom(Vector2.zero, rooms[level].BeginRoom));
        begin.Room.IsUsed = true;

        // Создание путей.
        for (int i = 0; i < 10; i++) CreateWay(new Vector2(1, 1), new Vector2(5, 5), null);

        CreateWay(new Vector2(5, 5), new Vector2(8, 8), null);

        DoorCreator();

        if (onEndGenerate != null) onEndGenerate.Invoke();

        level++;
    }

    /// <summary>
    ///
    /// </summary>
    private void CreateWay(Vector2 dotMin, Vector2 dotMax, Room targetRoom)
    {
        int x;
        int y;
        int xDirection = 1, yDirection = 1;
        do
        {
            x = (int)Random.Range(dotMin.x, dotMax.x + 1);
            y = (int)Random.Range(dotMin.y, dotMax.y + 1);

            x = x + (x % 2);
            y = y + (y % 2);

            // Рандомизируем направление по обеем осям.
            if (Random.Range(0, 2) > 0) xDirection *= -1;
            if (Random.Range(0, 2) > 0) yDirection *= -1;


            y *= yDirection;
            x *= xDirection;

        }
        while (Mathf.Abs(x) == Mathf.Abs(y) && (floor[x + (Grid.XMax / 2), y + (Grid.YMax / 2)] == null || !floor[x + (Grid.XMax / 2), y + (Grid.YMax / 2)].IsReserved));



        Marker endDot = new Marker(x, y);
        Marker marker = new Marker();

        Room newRoom = null;
        int i = 0;
        do
        {
            // Рандомизируем ось, по которой будет генерироваться путь.
            // true == x : false == y;
            bool direct = true;
            direct = (Mathf.Abs(endDot.X - marker.X) > Mathf.Abs(endDot.Y - marker.Y));

            int stepsCount = 0;
            if (direct) stepsCount = Mathf.Abs(endDot.X - marker.X) - (Mathf.Abs(endDot.Y - marker.Y) - 1);
            else stepsCount = Mathf.Abs(endDot.Y - marker.Y) - (Mathf.Abs(endDot.X - marker.X) - 1);


            for (int step = 0; step < stepsCount; step++)
            {
                if (direct) marker.X += xDirection;
                else marker.Y += yDirection;
                if (floor[marker.X + (Grid.XMax / 2), marker.Y + (Grid.YMax / 2)] == null || !floor[marker.X + (Grid.XMax / 2), marker.Y + (Grid.YMax / 2)].IsReserved)
                    floor[marker.X + (Grid.XMax / 2), marker.Y + (Grid.YMax / 2)] = new Cell(newRoom = CreateRoom(new Vector2(marker.X * 23, marker.Y * 14)));
                if (newRoom != null) newRoom.IsUsed = false;
            }

            if (i++ > 1000)
            {
                Debug.LogError($"Error: Infinity in create floor [CreateWay] cordinats:[{x},{y}]");
                break;
            }
        }
        while (marker != endDot);


        if (targetRoom != null) newRoom = CreateRoom(new Vector2(endDot.X * 23, endDot.Y * 14), targetRoom);
        else newRoom = CreateRoom(new Vector2(endDot.X * 23, endDot.Y * 14));

        if (floor[endDot.X + (Grid.XMax / 2), endDot.Y + (Grid.YMax / 2)] == null || !floor[endDot.X + (Grid.XMax / 2), endDot.Y + (Grid.YMax / 2)].IsReserved)
            floor[endDot.X + (Grid.XMax / 2), endDot.Y + (Grid.YMax / 2)] = new Cell(newRoom);
        newRoom.IsUsed = false;
    }

    /// <summary>
    ///
    /// </summary>
    private Room CreateRoom(Vector2 position)
    {
        Room newRoom = Instantiate(rooms[level].Next(), RoomsParent.transform, false);
        newRoom.transform.position = position;
        return newRoom;
    }

    /// <summary>
    ///
    /// </summary>
    private Room CreateRoom(Vector2 position, Room target)
    {
        Room newRoom = Instantiate(target, RoomsParent.transform, false);
        newRoom.transform.position = position;
        return newRoom;
    }

    public void Clear()
    {
        foreach (var el in floor.grid) if (el.IsReserved) Destroy(el.Room.gameObject);

        floor = new Grid();
    }

    private void DoorCreator()
    {
        for (int x = 0; x < Grid.XMax; x++)
        {
            for (int y = 0; y < Grid.YMax; y++)
            {
                //if (floor.grid[x, y] != null) Debug.Log($"floor.grid[x, y]: {floor.grid[x, y]} |  isReserved: {floor.grid[x, y].IsReserved}");
                if (floor[x, y] != null && floor[x, y].IsReserved)
                {

                    // ДОПИШИ !!!
                    Door door = null;
                    if ((x - 1 >= 0) && floor[x - 1, y] != null && floor[x - 1, y].IsReserved)
                    {
                        door = floor[x, y].Room.Doors.Single(el => el.name == "Door_Left");
                        door.gameObject.SetActive(true);
                        door.NextRoom = floor[x - 1, y].Room;
                    }
                    if ((x + 1 < Grid.XMax) && floor[x + 1, y] != null && floor[x + 1, y].IsReserved)
                    {
                        door = floor[x, y].Room.Doors.Single(el => el.name == "Door_Right");
                        door.gameObject.SetActive(true);
                        door.NextRoom = floor[x + 1, y].Room;
                    }
                    if ((y - 1 >= 0) && floor[x, y - 1] != null && floor[x, y - 1].IsReserved)
                    {
                        door = floor[x, y].Room.Doors.Single(el => el.name == "Door_Down");
                        door.gameObject.SetActive(true);
                        door.NextRoom = floor[x, y - 1].Room;
                    }
                    if ((y + 1 < Grid.YMax) && floor[x, y + 1] != null && floor[x, y + 1].IsReserved)
                    {
                        door = floor[x, y].Room.Doors.Single(el => el.name == "Door_Up");
                        door.gameObject.SetActive(true);
                        door.NextRoom = floor[x, y + 1].Room;
                    }
                }
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class Grid
    {
        public static int XMax = 20;
        public static int YMax = 20;

        public Cell[,] grid;

        public static int ArrayToMapX(int x)
        {
            return x - (XMax / 2);
        }

        public static int ArrayToMapY(int y)
        {
            return y - (YMax / 2);
        }

        public static int MapToArrayX(int x)
        {
            return x + (XMax / 2);
        }

        public static int MapToArrayY(int y)
        {
            return y + (YMax / 2);
        }

        public Grid()
        {
            grid = new Cell[XMax, YMax];
        }

        public Cell this[int x, int y]
        {
            get
            {
                return grid[x, y];
            }
            set
            {
                grid[x, y] = value;
            }
        }
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        instance = this;
        GenerateFloor();
    }

    #endregion Unity
}
