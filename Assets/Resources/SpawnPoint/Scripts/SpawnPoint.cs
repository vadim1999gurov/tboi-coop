﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    private MonstersList data;
    public MonstersList Data
    {
        get => data;
    }

    #endregion Parameters

    #region Methods

    public Monster Spawn(Transform parent, Vector3 position)
    {
        return Instantiate(Data.Next(), position, Quaternion.identity, parent);
    }

    #endregion Methods
}
