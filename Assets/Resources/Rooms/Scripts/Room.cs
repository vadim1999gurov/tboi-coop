﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Delegates;

[DisallowMultipleComponent]
sealed public class Room : MonoBehaviour
{
    #region Events
    public event Call OnClear;

    #endregion Events

    #region Parameters

    [SerializeField]
    private TypeRoom type = TypeRoom.Usual;
    public TypeRoom Type
    {
        get
        {
            return type;
        }
    }

    [SerializeField]
    private bool isUsed;
    public bool IsUsed
    {
        get
        {
            return isUsed;
        }
        set
        {
            isUsed = value;
            if (value) gameObject.SetActive(true);
            else gameObject.SetActive(false);
        }
    }

    #endregion Parameters

    #region State

    private static List<Room> all = new List<Room>();
    public static List<Room> All
    {
        get
        {
            return all;
        }
    }

    private List<Door> doors;
    public List<Door> Doors
    {
        get
        {
            return doors;
        }
    }

    private List<Monster> monsters;
    public List<Monster> Monsters
    {
        get
        {
            return monsters;
        }
    }


    private GameObject objects;
    public GameObject Objects
    {
        get
        {
            if (objects != null) return objects;
            return objects = GetComponentsInChildren<Transform>().Single(el => el.name == "Objects").gameObject;
        }
    }

    private GameObject monstersObj;
    public GameObject MonstersObj
    {
        get
        {
            if (monstersObj != null) return monstersObj;
            return monstersObj = GetComponentsInChildren<Transform>().Single(el => el.name == "Monsters").gameObject;
        }
    }

    private GameObject spawnPointObj;
    public GameObject SpawnPointObj
    {
        get
        {
            if (spawnPointObj != null) return spawnPointObj;
            return spawnPointObj = GetComponentsInChildren<Transform>()?.Single(el => el.name == "SpawnPoints")?.gameObject;
        }
    }

    private Rewarder rewarder;
    public Rewarder Rewarder
    {
        get
        {
            if (rewarder != null) return rewarder;
            Rewarder[] tmp = GetComponentsInChildren<Rewarder>();
            if (tmp.Length > 0) rewarder = tmp[0];
            if (rewarder != null) OnClear += delegate { rewarder.DropReward(transform); };
            return rewarder;
        }
    }

    private bool isCleared;
    public bool IsCleared
    {
        get
        {
            return isCleared;
        }
        set
        {
            if (!isCleared && value && OnClear != null) OnClear.Invoke();
            isCleared = value;
        }
    }


    #endregion State

    #region Methods

    private void OpenDoors()
    {

        foreach (var el in doors) el.IsOpen = true;
    }

    private void CloseDoors()
    {
        foreach (var el in doors) el.IsOpen = false;
    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        monsters = new List<Monster>();
        doors = new List<Door>();

        foreach (var el in GetComponentsInChildren<Transform>())
            if (el.name.Contains("Door_"))
            {
                Doors.Add(el.GetComponent<Door>());
                if (el.GetComponent<Door>() != null) el.GetComponent<Door>().CurrentRoom = this;
                el.gameObject.SetActive(false);
            };
        All.Add(this);

        if (MonstersObj != null) foreach (var el in SpawnPointObj.GetComponentsInChildren<SpawnPoint>())
            {
                Monster monsterTmp = el.Spawn(SpawnPointObj.transform, el.transform.position);
                Monsters.Add(monsterTmp);
                monsterTmp.OnDeath += delegate { Monsters.Remove(monsterTmp); if (Monsters.Count == 0) { IsCleared = true; OpenDoors(); } };
            }
        IsCleared = monsters.Count == 0;

        rewarder = Rewarder;
    }

    private void OnEnable()
    {
        if (IsCleared) OpenDoors();
        else CloseDoors();
    }

    private void OnDestroy()
    {
        All.Remove(this);
    }


    #endregion Unity
}


public enum TypeRoom
{
    Usual,
    Shop,
    Treasury,
    Boss
}
