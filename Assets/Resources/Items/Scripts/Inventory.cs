﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Delegates;

[DisallowMultipleComponent]
public class Inventory : MonoBehaviour
{

    #region Events

    public event Call OnEditData;

    #endregion Events

    #region State

    [System.NonSerialized]
    private List<ArtifactData> artifacts = new List<ArtifactData>();
    public List<ArtifactData> Artifacts
    {
        get
        {
            return artifacts;
        }
    }


    [SerializeField]
    private ItemData resources;
    public ItemData Resources
    {
        get
        {
            resources = resources ?? new ItemData();
            return resources;
        }
        set
        {
            Debug.Log($"Before: {resources.ToString()}| After: {value.ToString()}");
            resources = value;
            if (OnEditData != null) OnEditData.Invoke();
        }
    }

    #endregion State

}
