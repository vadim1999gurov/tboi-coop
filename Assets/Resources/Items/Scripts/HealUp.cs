﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealUp : MonoBehaviour
{
	#region Parameters

    [SerializeField]
    [Tooltip("Character health.")]
    private float value;
    /// <summary>
    /// Heal up items.
    /// </summary>
    public float Value
    {
        get
        {
            return this.value;
        }
        set
        {
            if(value >= 0) this.value = value;
        }
    }

	#endregion Parameters

	
	#region Methods

	public float GetHealth()
	{
		Destroy(this.gameObject);
		return Value;
	}

	#endregion
}
