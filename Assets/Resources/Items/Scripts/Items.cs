﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[ExecuteInEditMode]
public class Items : MonoBehaviour 
{
	#region Parameters

	[SerializeField]
	private List<ItemData> all;
	public List<ItemData> All
	{
		get
		{
			return all;
		}
	}

	#endregion Parameters

	#region Methods

	public ItemData GetRandomItem()
	{
		return all[Random.Range(0, All.Count)];
	}

	#endregion Methods

	#region State

	public static Items Instance = null;

	#endregion State

	#region Unity

	private void Awake()
	{
		if(!gameObject.name.Contains("(Singleton)")) gameObject.name += "(Singleton)";
		if(Instance == null) Instance = this;
	}

	#endregion Unity
}
