﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
//[RequireComponent(typeof(PolygonCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]

sealed public class Item : MonoBehaviour
{
    #region Parameters

    [SerializeField]
    [Tooltip("Item data.")]
    private ItemData data;
    /// <summary>
    ///	Item data.
    /// </summary>
    public ItemData Data
    {
        get
        {
            return data;
        }
        set
        {
            data = value;
            Icon.sprite = data.Icon;
        }
    }

    // TODO: Need add UI controller.
    #endregion


    #region State

    [System.NonSerialized]
    private SpriteRenderer icon;
    /// <summary>
    ///	Item data.
    /// </summary>
    public SpriteRenderer Icon
    {
        get
        {
            icon = icon ?? GetComponent<SpriteRenderer>();
            return icon;
        }
        set
        {
            icon = value;
        }
    }

    #endregion State 


    #region Unity

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Inventory target = collision.gameObject.GetComponent<Inventory>();

        if (target != null)
        {
            target.Resources += Data;
            Destroy(this.gameObject);
        }
    }

    private void OnDestroy()
    {

    }

    private void Start()
    {
        Icon = Icon;
        Data = Items.Instance.GetRandomItem();
        this.gameObject.AddComponent(typeof(PolygonCollider2D));
    }

    #endregion Unity
}
