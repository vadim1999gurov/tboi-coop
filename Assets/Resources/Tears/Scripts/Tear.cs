﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(DamageDealer))]
[RequireComponent(typeof(CircleCollider2D))]
sealed public class Tear : MonoBehaviour
{
    #region Flags

    private bool faced = false;

    #endregion Flags

    #region Constans

    // Character min and max speed stat.
    private const float MinSpeed = 1f;
    private const float MaxSpeed = 10f;

    #endregion Constans

    #region State

    [System.NonSerialized]
    private float speed = MinSpeed;
    /// <summary>
    /// Character bullet speed.
    /// </summary>
    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            if (MinSpeed <= value && value <= MaxSpeed) speed = value;
        }
    }

    [System.NonSerialized]
    private Vector3 target;
    /// <summary>
    /// Target position.
    /// </summary>
    public Vector3 Target
    {
        get
        {
            return target;
        }
        set
        {
            target = value;
        }
    }

    [System.NonSerialized]
    private DamageDealer damageDealer;
    public DamageDealer DamageDealer
    {
        get
        {
            if (damageDealer != null) return damageDealer;
            return damageDealer = GetComponent<DamageDealer>();
        }
    }

    [System.NonSerialized]
    new private CircleCollider2D collider;
    public CircleCollider2D Collider
    {
        get
        {
            if (collider != null) return collider;
            return collider = GetComponent<CircleCollider2D>();
        }
    }

    [System.NonSerialized]
    private string nameCreatorObject;
    public string NameCreatorObject
    {
        get
        {
            return nameCreatorObject;
        }

        set
        {
            nameCreatorObject = value;
        }
    }

    #endregion State

    #region Methods

    private void Move(Vector3 target, float speed)
    {
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if (target == transform.position) Bloop();
    }

    #endregion Methods

    #region Unity

    private void FixedUpdate()
    {
        if (!faced) Move(Target, Speed);
    }

    private void Awake()
    {
        Collider.isTrigger = false;
    }

    private void Bloop()
    {
        Collider.enabled = false;
        Animator anim = GetComponent<Animator>();
        anim.SetBool("isBlooped", true);

        AnimatorStateInfo animationState = anim.GetCurrentAnimatorStateInfo(0);
        AnimatorClipInfo[] myAnimatorClip = anim.GetCurrentAnimatorClipInfo(0);

        Destroy(this.gameObject, 0.5f);

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Tear>() == null)
        {
            faced = true;
            Bloop();
        }
    }

    #endregion Unity
}
