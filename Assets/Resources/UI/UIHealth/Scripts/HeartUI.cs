﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartUI : MonoBehaviour
{
    #region State

    [SerializeField]
    private HeartData data;
    public HeartData Data
    {
        get => data;
        set
        {
            data = value;
            if (data != null)
            {
                Image.sprite = data.Icon;
            }
        }
    }

    [SerializeField]
    private Image image;
    public Image Image
    {
        get => image ?? (image = GetComponent<Image>());
    }

    #endregion State
}
