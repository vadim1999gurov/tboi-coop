﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeartsContainer : MonoBehaviour
{
    #region Constants
    private const string pathResources = "UI/UIHealth/Prefabs/";

    #endregion Constants

    #region Parameters

    [SerializeField]
    private Health targetHealth;

    [SerializeField]
    private bool isLogging = false;

    #endregion Parameters

    #region State
    // Для дэбага.
    [SerializeField]
    private List<HeartUI> hearts;
    public List<HeartUI> Hearts
    {
        get => hearts;
    }

    private DamageReceiver damageReceiver;
    public DamageReceiver DamageReceiver
    {
        get
        {
            return damageReceiver;
        }
        set
        {
            damageReceiver = value;
        }
    }

    #endregion State



    #region Methods

    private void Clear()
    {
        foreach (var el in Hearts) Destroy(el.gameObject);
        hearts = new List<HeartUI>();
    }

    private void UpdateUI()
    {
        Clear();

        Log("UI is updated");

        if (targetHealth.MaxValue == targetHealth.Value)
        {
            for (int i = 0; i < targetHealth.MaxValue; i++) Hearts.Add(GetHeart("FullHeart"));
            return;
        }

        int fullHearts = (int)targetHealth.Value;
        int halfHeart = (targetHealth.Value % 1) > 0 ? 1 : 0;
        int emptyHearts = (int)(targetHealth.MaxValue - targetHealth.Value);


        for (int i = 0; i < fullHearts; i++) hearts.Add(GetHeart("FullHeart"));
        if (halfHeart > 0) hearts.Add(GetHeart("HalfHeart"));
        for (int i = 0; i < emptyHearts; i++) hearts.Add(GetHeart("EmptyHeart"));

    }


    private HeartUI GetHeart(string name)
    {
        HeartUI tmp = (HeartUI)Resources.Load<HeartUI>(pathResources + "Heart");
        if (tmp == null)
        {
            Debug.LogError("HeartUI wasn't found");
            return null;
        }
        tmp.Data = (HeartData)Resources.Load<HeartData>(pathResources + name);
        return Instantiate<HeartUI>(tmp, transform, false);
    }

    private void Log(string message)
    {
        if (isLogging) Debug.Log(message);
    }

    #endregion Methods


    #region Unity

    private void Awake()
    {
        hearts = new List<HeartUI>();
    }

    private void Start()
    {
        if (targetHealth != null) DamageReceiver = targetHealth.GetComponent<DamageReceiver>();
        else
        {
            Debug.LogError("NullReferenceError: Target health reference is empty.");

            return;
        }

        targetHealth.onHealthUpdate += UpdateUI;

        UpdateUI();
    }

    private void OnDestroy()
    {
        targetHealth.onHealthUpdate -= UpdateUI;

    }

    #endregion Unity
}