﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "Heart", menuName = "UI/Heart", order = 0)]
public class HeartData : ScriptableObject
{
    [SerializeField]
    private Sprite icon;
    public Sprite Icon
    {
        get => icon;
    }

    [SerializeField]
    private int order;
    public int Order
    {
        get => order;
    }

    public object Clone()
    {
        HeartData tmpClone = HeartData.CreateInstance<HeartData>();
        tmpClone.icon = this.icon;
        tmpClone.order = this.order;
        return tmpClone;
    }
}
