﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Door : MonoBehaviour
{
    #region State

    private Room nextRoom;
    public Room NextRoom
    {
        get
        {
            return nextRoom;
        }
        set
        {
            if (value == null) gameObject.SetActive(false);
            nextRoom = value;
            ChangeDecore();
        }
    }

    private Room currentRoom;
    public Room CurrentRoom
    {
        get
        {
            return currentRoom;
        }
        set
        {
            if (value == null) gameObject.SetActive(false);
            currentRoom = value;
            ChangeDecore();
        }
    }

    private SpriteRenderer borderDoor;
    private SpriteRenderer back;
    private SpriteRenderer sashLeft;
    private SpriteRenderer sashRight;

    private SpriteMask mask;

    private UIMoverOnTime2D sashLeftAnimation;
    private UIMoverOnTime2D sashRightAnimation;


    [SerializeField]
    private bool isOpen = false;
    public bool IsOpen
    {
        get
        {
            return isOpen;
        }
        set
        {
            if (isOpen != value)
            {
                isOpen = value;
                if (isOpen) Open();
                else Close();
            }

        }
    }

    #endregion State


    #region Methods

    private void ChangeDecore()
    {
        if (NextRoom == null) return;
        Sprite[] decor = null;

        bool isBoss = false;

        if (NextRoom.Type == TypeRoom.Usual && CurrentRoom.Type == TypeRoom.Usual) return;
        else if (NextRoom.Type == TypeRoom.Treasury || CurrentRoom.Type == TypeRoom.Treasury) decor = Resources.LoadAll<Sprite>("Objects/Door/Sprites/door_treasury");
        else if (NextRoom.Type == TypeRoom.Boss || CurrentRoom.Type == TypeRoom.Boss)
        {
            decor = Resources.LoadAll<Sprite>("Objects/Door/Sprites/door_boss");
            isBoss = true;
        }

        if (decor != null)
        {
            borderDoor.sprite = decor.Single(el => el.name == "border");

            back.sprite = decor.Single(el => el.name == "back");
            mask.sprite = back.sprite;

            sashLeft.sprite = decor.Single(el => el.name == "left");
            sashRight.sprite = decor.Single(el => el.name == "right");

            // Корректировка положения спрайтов.
            if (isBoss)
            {
                sashLeft.transform.parent.transform.localPosition = new Vector3(sashLeft.transform.parent.transform.localPosition.x, -0.3f, sashLeft.transform.parent.transform.localPosition.z);

                back.transform.localPosition = new Vector3(sashLeft.transform.parent.transform.localPosition.x, 0.4f, sashLeft.transform.parent.transform.localPosition.z);
                back.transform.localScale = new Vector3(sashLeft.transform.parent.transform.localScale.x + 0.5f, sashLeft.transform.parent.transform.localScale.x, sashLeft.transform.parent.transform.localScale.z);

                mask.transform.localScale = new Vector3(sashLeft.transform.parent.transform.localScale.x + 0.5f, sashLeft.transform.parent.transform.localScale.x, sashLeft.transform.parent.transform.localScale.z);
            }
        }
    }

    private void TransferCharacter(Character target)
    {
        string str = "";
        Vector3 transferPosition = Vector3.zero;
        if (gameObject.name.Contains(str = "Up"))
        {
            transferPosition = nextRoom.Doors.Single(el => el.name.Contains("Down")).transform.position;
            transferPosition.y += 0.5f;

        }
        else if (gameObject.name.Contains(str = "Left"))
        {
            transferPosition = nextRoom.Doors.Single(el => el.name.Contains("Right")).transform.position;
            transferPosition.x += -0.5f;
        }
        else if (gameObject.name.Contains(str = "Right"))
        {
            transferPosition = nextRoom.Doors.Single(el => el.name.Contains("Left")).transform.position;
            transferPosition.x += 0.5f;
        }
        else if (gameObject.name.Contains(str = "Down"))
        {
            transferPosition = nextRoom.Doors.Single(el => el.name.Contains("Up")).transform.position;
            transferPosition.y += -1.3f;
        }
        target.transform.position = transferPosition;
        MiniMap.Instance.MoveTo(str);
        // Включаем следующую комнату. 
        NextRoom.IsUsed = true;
        Camera.main.GetComponent<UIMover2D>().StartAnimation(NextRoom.transform.position, delegate { CurrentRoom.IsUsed = false; });
    }

    private void Open()
    {

        if (sashLeftAnimation != null && sashRightAnimation != null)
        {
            sashLeftAnimation.StartAnimation();
            sashRightAnimation.StartAnimation();
            if (GetComponent<Collider>()) GetComponent<Collider>().enabled = true;
        }
    }

    private void Close()
    {
        if (sashLeftAnimation != null && sashRightAnimation != null)
        {
            sashLeftAnimation.StartReverseAnimation();
            sashRightAnimation.StartReverseAnimation();
            if (GetComponent<Collider>()) GetComponent<Collider>().enabled = false;
        }

    }

    #endregion Methods

    #region Unity

    private void Awake()
    {
        foreach (var el in GetComponentsInChildren<Transform>())
        {
            if (el.name == "BorderDoor") borderDoor = el.GetComponent<SpriteRenderer>();
            if (el.name == "Back") back = el.GetComponent<SpriteRenderer>();
            if (el.name == "SashLeft")
            {
                sashLeft = el.GetComponent<SpriteRenderer>();
                sashLeftAnimation = el.GetComponent<UIMoverOnTime2D>();
                sashLeftAnimation.TimeReverseAnimation = sashLeftAnimation.TimeAnimation;
            }
            if (el.name == "SashRight")
            {
                sashRight = el.GetComponent<SpriteRenderer>();
                sashRightAnimation = el.GetComponent<UIMoverOnTime2D>();
                sashRightAnimation.TimeReverseAnimation = sashRightAnimation.TimeAnimation;
            }
            if (el.name == "Mask") mask = el.GetComponent<SpriteMask>();
        }
    }


    private void Start()
    {
        if (isOpen) Open();
        else Close();
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (isOpen) Open();
        else Close();
    }

#endif

    private void OnCollisionEnter2D(Collision2D col)
    {
        Character character = col.gameObject.GetComponent<Character>();
        if (isOpen && character != null) TransferCharacter(character);
    }


    #endregion Unity
}
