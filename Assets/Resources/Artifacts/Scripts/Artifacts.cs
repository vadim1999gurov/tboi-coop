﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[DisallowMultipleComponent]
public class Artifacts : MonoBehaviour 
{

	#region Parameters

	[SerializeField]
	private List<ArtifactData> all;
	public List<ArtifactData> All
	{
		get
		{
			return all;
		}
	}

	#endregion Parameters

	#region Methods

	public ArtifactData GetRandomArtifact()
	{
		return all[Random.Range(0, All.Count)];
	}

	#endregion Methods

	#region State

	public static Artifacts Instance = null;

	#endregion State

	#region Unity

	private void Awake()
	{
		if(!gameObject.name.Contains("(Singleton)")) gameObject.name += "(Singleton)";
		if(Instance == null) Instance = this;
	}

	#endregion Unity
}
