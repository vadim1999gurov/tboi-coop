﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Artifact : MonoBehaviour 
{
	#region State

	[SerializeField]
	[Tooltip("Artifact data.")]
	private ArtifactData data;
	/// <summary>
	///	Item data.
	/// </summary>
	public ArtifactData Data
	{
		get
		{
			return data;
		}
		set
		{
			data = value;
			Icon.sprite = data.Icon;
		}
	}

	[System.NonSerialized]
	private SpriteRenderer icon;
	/// <summary>
	///	Artifact icon.
	/// </summary>
	public SpriteRenderer Icon
	{
		get
		{
			icon = icon ?? GetComponent<SpriteRenderer>();
			return icon;
		}
		set
		{
			icon = value;
		}
	}

	#region Unity

	private void Update()
	{
		
	}

	private void Start()
	{
		Icon = Icon;
		Data = Artifacts.Instance.GetRandomArtifact();
		this.gameObject.AddComponent(typeof(PolygonCollider2D));
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		Inventory target = collision.gameObject.GetComponent<Inventory>();

		if (target != null)
		{
			//? Тут он копирует данные и если поднять артефакт, и он дает ноль здоровья, то перс умирает.
			target.GetComponent<Character>().AllStats += Data.Stats;
			target.Artifacts.Add(Data);
			Destroy(this.gameObject);
		} 
	}

	#endregion Unity

	#endregion State
}
