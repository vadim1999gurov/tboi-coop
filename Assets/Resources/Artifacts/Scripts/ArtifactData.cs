﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Artifact", menuName = "Structs/Artifact", order = 1)]
public class ArtifactData : ScriptableObject
{
    #region Parameters
    [SerializeField]
    private new string name;
    public string Name
    {
        get
        {
            return name;
        }
    }

    [SerializeField]
    private string description;
    public string Description
    {
        get
        {
            return description;
        }
    }

    [SerializeField]
    private Sprite icon;
    public Sprite Icon
    {
        get
        {
            return icon;
        }
    }

    [SerializeField]
    private Stats stats;
    public Stats Stats
    {
        get
        {
            return stats;
        }
    }

    [SerializeField]
    private GameObject effect;
    public GameObject Effect
    {
        get
        {
            return effect;
        }
    }

    #endregion Parameters
}
